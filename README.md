# Eonpass

![Eonpass](assets/logo.jpg)

Eonpass is the open source protocol to handle goods identification and change of holders on blockchains. It was originally designed by the infamous team Cryptomice at EU Blockathon 2018 where it won the final prize.

## Brief Summary
Fake goods are injected in the markets everyday, they represent both sale loss for legitimate intellectual property holders and potentally hazard for the consumers. In this context Eonpass protocol was designed to create a virtual world where goods are checked before the real world. The basic idea is that the receiver doesn't accept a physical good if the seller cannot demonstrate and pass holdership of the virtual one. Detecting illicits before they are actually in the hands of the new holder is very important because new holders may be held liable when they accept the fake goods. This is the case for Logistic Operators which are held liable for dangerous goods as soon as they take the cargo in charge within one of their facilities.

## Document Structure
The document is organized as follows:

- [Architecture](https://gitlab.com/Eonpass/specs/blob/master/architecture.md)
- [Bitcoin Implementation](https://gitlab.com/Eonpass/bitcoin-implementation)
- [Ethereum Implementation](#)
- [Special Thanks](#)