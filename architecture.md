# Protocol Architecture

In this section we describe the functions that compose the protocol.

## Actors

1. **Brand Owners**, IP holders are the registered owners of a brand within EUIPO or other insitituions. They must be the only party able to emit the tokens.
2. **Producers**, They represent factories that actually assemble/build the products for a brand. Usually they are not the owner of the all the brands they produce, therefore they must assess authenticity of the production order but cannot modify production quantities.
3. **Logistics Operators**, Carriers moving the goods between places. They must assess the authenticity of goods before hangling the shipments because once they take a shipment in charge they are liable for eventual violations.
4. **Customs**, Customes perform sample checks on goods entering/exiting a country or econimic community.
5. **Distribution**, Set of warehouses and shops where the goods are displayed for the consumers. They must assess the origin of the received products and let consumers check that origin too.
6. **Consumers**, General public, they may want to check the authenticity of goods or may be required to prove authenticity to access customer service, reparis, insurances, etc.
7. **Customer Service, Insurance and other services**, Business entities handling consumers requests or selling additional services like insurances. They must assess that the product subject of the request is authentic.

![Actors](assets/actors.jpg)

## Process

<img align="right" src="/assets/IPO.jpg" alt="Intellectual Property Owners" width="450"/>

- A _Brand Owner_ wants to produce 100 objects of product X and emits 100 virtual tokens
- The _Brand Owner_ sends the production order along with the proof of holdership of the virtual tokens to the _Producer_
- The _Producer_ **before** accepting the order and the tokens checks if the _Brand Owner_ can prove holdership of the virtual tokens
  - If the _Brand Owner_ can't prove the holdership, the order is refused (and anyways the fake _Brand Owner_ cannot pass holdership). Moreover a warning signal is raised tagging the defecting entity.
  - If the _Brand Owner_ proves holdership, the order is accepted and the tokens change holder (i.e. the _Producer_ is the current holder of the 100 tokens)
- When the production is completed, the _Producer_ sends logicsts details and the proof of holdership of the tokens to a _Logistics Operator_

<hr/>


<img align="right" src="/assets/logistics.jpg" alt="Logistics Operators" width="450"/>

- The _Logistics Operator_ **before** accepting the shipment and the tokens checks if the _Producer_ can prove holdership of the virtual tokens
  - If the _Producer_ can't prove the holdership, the order is refused (and anyways the fakes _Producer_ cannot pass holdership). Moreover a warning signal is raised tagging the defecting entity.
  - If the _Producer_ proves holdership, the shipment is accepted and the tokens change holder (i.e. the _Logistics Operator_ is the current holder of the 100 tokens)
- The _Logistcs Operator_ can transfer the goods and the virtual tokens to other _Logistics Operators_ or _Customs_ with the same mechanism: prove holdership of tokens upfront, then transfer the goods and the tokens. In turn the receiving entity will perform the same procedure.
- Product lots and shipments are an aggregation of more tokens which can be handled in batch. See below and reference implementation for examples.

<hr/>

<img align="right" src="/assets/customs.jpg" alt="Customs" width="450"/>

- When _Customs_ accept a shipment they can follow backwards the chain of previous holders and compare it with usual holders for a given good. This and other info will be compacted and stored in the blockchain.
- At the end of the logistics chain there is _Distribution_ accepting the goods and bringing it to display for _Consumers_. _Distribution_ will also follow the "accept only if token holdership is proved" policy to accept/decline shipments.

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

<hr/>

- _Consumers_ in shops will be able to check the virtual token and see if its information match the information of the product they have in hands. This may be their will or may be required to activate insurances, warranties and access customer service. _Consumers_ may have their wallet to accept tokens or may resort to third parties wallet granting their right to be forgotten.
- _Customer Service, Insurances, Warranties and others_ may require the virtual tokens before granting their services and anyway they can check the chain of holders, product information and see if they match with the clients'requests.

## Definitions


### Address
A unique public string representing a real-world entity which will tell the address to receive tokens. Receiving tokens is a passive activity, sending tokens requires the knowledge of the private key associated with the address currently holding the tokens. The private key is used to craft a transaction towards the destination address. For instance in the early days of Bitcoin the public key was directly the address, the transactions were in fact _pay to public key_. To improve security Bitcoin moved to _pay to public key hash_, where the address of the receiving end of a transaction is specified as the hash of its public key.

    Bitcoin Address = RIPMED(SHA256(Public Key))

Using the standard cryptographic functions [SHA256](https://en.wikipedia.org/wiki/SHA-2) and [RIPMED-160](https://en.wikipedia.org/wiki/RIPEMD).

### Wallet
Software that manages addresses, their keys and tokens. It allows users to send and receive tokens with a simple UX. The wallets should be open-source and the keys should be stored locally, all other variations require trust in third parties and may be adopted if fitting the business case.
Wallets can also be in charge of generating hierarchical keys from a seed phrase. This means that a single phrase can generate fresh public/private key pairs which can be used for different transactions. 

### Signature
A signature is a mathematical scheme to prove that an actor signed a message. A common scheme is based on RSA and uses public and private keys. The public key is shared and used to prove that only the wner of the private key could have signed that message.

### Transaction
A cryptographically secure operation which updates the accounting balance of wallets. Transactions are grouped in blocks and are part of the mathematical challenge issued to miners. Bitcoin-like architecture will hace the concept of Unspent and Spent transaction outputs (see meaning of [UTXO](https://en.wikipedia.org/wiki/Unspent_transaction_output)) whereas in Ethereum-like enviornment we may use or adapt an implementation which has similar properties:
- an unspent transaction output can be used only by its owner (i.e. in possess of the correspinding private keys)
- a UTXO has a unique identifier which can be used to retrieve from a public source (e.g. UTXOs pool in Bitcoin)
- once a UTXO is spent it can no longer be spent again
In Ethereum the closest and easiest approach would be to use ERC-721 where _Transfer_ is the spending action and _Approve_ is the demonstration of UTXO existance.

### Token
The representation of a single object, in the current draft it is a file with the serial number of the object and other characteristics of the object. The file is signed by the brand owner. The hash will be used as a mean of proving the existance of a certain object without necessarily discolsing all of its information.

### Contract
An Eonpass contract is a list of hash of tokens, their respective serial number and a transaction of the Brand Owner on a chosen blockchain (in the current example we refer to Bitcoin). This tuple is written to file and signed by the brand owner. This document represent a lot of production and passing the holdership of this document corresponds to passing the holdership of all the items inside the document. Contracts can be split so that a subset of the tokens can be passed to an holder and the rest to another holder.

### Proof of Holdership
An actor needs to show to the future receiver that he has access to the private key of the address currently holding the tokens. There a couple of ways to achieve this:
- in the current description the receiver must receive enough information to be able to reconstruct the whole history of holders from the original brand owner down to the current sender of the tokens.
- in ERC-721 it corresponds to the _Approval_ event, where the current holder approves the transfer of a token to the address of the future holder. Only the current holder can do this operation.
- more in general the sender must prove that he is in possession of the private key, which can be done by signing a content with the private key of the public key corresponding to the address currently holding the tokens. An example would be to sign the hash of the current token and the receiver address, the receiver cannot ask to sign just anything because this technique can be used to learn information on the private key.

### Transfer Holdership
When an actor transfers his/her tokens to another actor, this other party becomes the new holder. According to Eonpass protocol the transfer should happen only after the Proof of Holdership is done, i.e. after the receiver acknoledges that he/she is willing to accept the incoming tokens.
- the implementation of the protocol must make sure that the Proof of Holdership is done before the transfer. 
- in ERC-721 it corresponds to the _Transfer_ event, in the current description it corresponds to the spending of a pre-declared transaction that all interested witnesses can see changing from unspent to spent.
Tokens transferred without the proof of holdership will automatically rise a warning flag on the entity sending them.


## Data Structures

### Contract
A list of hases of the token files, their serial numbers and a transaction by the Brand Owner, this is the "origination transaction". When this transaction is spent, the chain of holders begins from the brand owner.

### Pre-flight Proof of Contract
The data needed by the receiver to validate the authenticity and the history of the contract. This is composed by the contract file, an unspent transaction "current seal" which will be spent to transfer holdership of the contract, the list of objects. Notice that inside the contract there is the "origination transaction", this transaction will be the begininng of an exploration problem to find a path between the origination transaction and the "current seal" transaction.

### Proof of Contract
A document which proves that the receiver had all the information needed to analyze the contract to assess its history. It is composed of the "current seal" transaction, the contract and the "next seal" transaction. This information is signed by the receiver. The "current seal" is the transaction that the current holder needs to spend to pass holdership, the "next seal" is an unspent transaction of the receiver which will be the future "current seal" when he/she will need to pass holdership of the contract.

### Comments on the Data Structures
- The proof of holdership is the ability to find a path from the origination transaction to the current seal transaction. This path though is not necessarily between transaction, in fact it can be even across blockchains. In the OP_RETURN of each spent transaction there will be the hash of the proof of contract. In the proof of contract you there will be the "next seal" which can be identified by a transaction id and a blockchain id. This recursive exploration is the idea behind the proof of holdership and is explain in full in the chapter "Processes".
- A receriver confirms that he/she is willing to receive the hodlership by signing the Proof of Contract, if the receiver is not willing to accept the holdership of the contract he/she will not sign the Proof of Contract. In this scenario the sender can't create a compliant Proof of Contract.
- Signatures require public key sharing, we currently advise to declare a list of wallets and public keys belonging to an entity on one of her certified websites.


## Processes

### Virtual Twin creation and dispatch to first receiver
This process is specific for Brand Owners and the first receiver, which may be a Production facility. The brand owners are the only ones who can emit Virtual Twins connected to their brands. To do so they share with the public a list of wallets or a pool of Unspent Transactions.

![Virtual Twin Creation](assets/Virtual_Twin_Creation_and_Dispatch.png)

### Passing Holdership from generic sender to receiver
Two parties follows this procedure to acknowledge the change of ownership of a contract from the current holder to the next holder. The receiving party needs to double check the authenticity of the contract and the identity of the current holder (see next process)

![Passing Holdership](assets/Passing_Holdership_between_two_parties.png)

### Proof of Holdership
Before accepting the virtual twins the receiving end reconstructs the history of holders up to the current hodler and matches its identity with the party he/she is dealing with. The first instance will be an exploration from the origin to the current branch of the holders, if the future receiver can't find a path from the original transaction down to the current holder then he/she will not create the Proof of Contract and the sender will not be able to pass on hodlership.

![Proof of Holdership](assets/Proof_of_Holdership.png)

### Splitting Contracts
A production lot may need to be split for delivery in sublots. This is the process to peform a contract split which can be verified by future holders down the line.

![Splitting Contracts](assets/Splitting_Contracts.png)

### Comments on Processes
- Joining Contracts will also be described, but for the first intance we focus on the minimum set of features to have the protocol run. Splitting contracts (i.e. send 10 objects one way, 10 objects the other way) is mandatory, but sending two lots the same way can be done by just sending the two lots to the same receiver.
- The current description of Proof of Holders is a depth or width first exploration, it is trivial to improve it with a brench and bound approach so that the exploration follows first the branches where the objects in the inspected contract are. Notice that the receiver may want to explore all the branches anyways to be sure that the object he/she is receiving are not duplicated by error of by malicous users somewhere else in the graph.
- By feasible contents of the contract we intend that every object has been created by the proper brand owner and has never been duplicated in the whole line of holders.






